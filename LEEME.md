

## Similar projects

    https://github.com/osixia/docker-light-baseimage


 printf "%s\n"      | debconf-set-selections 

debconf-set-selections can be used to pre-seed the debconf database with answers, or to change answers in the database. Each question will be marked as seen to prevent debconf from asking the question interactively. 



## apt-cacher-ng

debootstrap no usa el cache, usa wget

to force debootstrap to use the proxy, an environment variable is set:

	export http_proxy=http://10.88.0.2:3142 

But debootstrap run with the error:

	E: Failed getting release file http://snapshot.debian.org/archive/debian/20221111T180000Z/dists/bullseye/Release

This is because wget launch this error:

	
	wget http://snapshot.debian.org/archive/debian/20221111T180000Z/dists/bullseye/Release
	URL transformed to HTTPS due to an HSTS policy
	--2022-12-28 02:09:41--  https://snapshot.debian.org/archive/debian/20221111T180000Z/dists/bullseye/Release
	Connecting to 10.88.0.2:3142... connected.
	Proxy tunneling failed: CONNECT denied (ask the admin to allow HTTPS tunnels)Unable to establish SSL connection.

So, apt-cacher-ng must to admit a tunnel conexction from Release files

I try to do this throw PassThroughPattern variable but none of this seems to work:

	PassThroughPattern: snapshots.debian.org:443
	PassThroughPattern: snapshots.debian.org/.*
	PassThroughPattern: https://snapshots.debian.org
	PassThroughPattern: https:.*

but
	PassThroughPattern: snapshots.debian.org

The problem is that with the last pattern there are no cache.

So, for now there are no cache for debootstrap

## --no-merged-usr

Hay un problema con 


## multi-achitecture



Se crea la imagen con la arquictura como etiqueta
CODENAME=bullseye

podman build
        -t registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-${ARCH}  \


Se sube al registro


podman push registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-${ARCH}


Así en cada arquitectura

podman manifest create debian-bullseye-slim
podman manifest add     debian-bullseye-slim registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-arm
podman manifest add     debian-bullseye-slim registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-amd64
podman manifest push    debian-bullseye-slim registry.conexo.mx/kaxtli/debian:${CODENAME}-slim
podman image rm debian-bullseye-slim


Hay un error en el que se crea la imagen  debian-bullseye-slim  y no se puede listar, pues el archivo no existe ( hay que borrar esa imagen )


