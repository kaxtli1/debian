# 2022-11-10


#CODENAME := bullseye
CODENAME := bookworm

DATE :=  $(shell date +%Y%m%d)
DATEF := 2022-12-16T00:00:00Z
DATEF := 2022-11-12T00:00:00Z
DATEF := 2023-04-22T00:00:00Z
DATEF :=  $(shell date +%F)T23:00:00Z

PWD := /root/kaxtli/debian/
ARCH := $(shell podman system info | grep OsArch | sed 's#.*/##')

build: build-full build-slim

build-slim: rootfs-slim.tmp

	    # It use squid as a proxy
	#    export http_proxy=http://localhost:3182
	ls
	[ -d rootfs.tmp ] && rm rootfs.tmp || true
	echo Bn
	ln -s rootfs-slim.tmp rootfs.tmp
	podman build \
		-t localhost/debian:${CODENAME}-slim  \
		-t registry.local/kaxtli/debian:${CODENAME}-slim  \
		-t registry.conexo.mx/kaxtli/debian:${CODENAME}-slim  \
		-t registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-${ARCH}  \
		-t registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-$(shell date --date @$(shell cat rootfs-slim.tmp/debuerreotype-epoch ) +%Y%m%d ) \
		-f Containerfile

build-full: rootfs-full.tmp

	[ -d rootfs.tmp ] && rm rootfs.tmp || true
	ln -s rootfs-full.tmp rootfs.tmp
	podman build \
		-t registry.conexo.mx/kaxtli/debian:${CODENAME}  \
		-t registry.conexo.mx/kaxtli/debian:${CODENAME}-${DATE} \
		-f Containerfile

    # squid is used only for deburreotype
    export http_proxy=
cache:
	# descarga los paquetes .deb
	mkdir cache
	debootstrap --download-only --force-check-gpg --variant=minbase --cache-dir=${PWD}/cache ${CODENAME} rootfs.deb/
	rm -fr rootfs.deb/
	# debuerroetype no puede usar el cache, así que esta sección es inutil.
	# no acepta el parametro --cache-dir
	# no utiliza un rootfs que tenga archivos

rootfs-slim.tmp:
	rm -fr rootfs-slim.tmp/


	# Se asegura que se use https
	# se configura apt-cacher-gn Remap-debrep: http://snapshot.debian.org/  https://snapshot.debian.org/
	#sed -i 's#http://#https://#' /usr/share/debuerreotype/scripts/.snapshot-url.sh
	#http_proxy=http://10.88.0.2:3142 debuerreotype-init --no-merged-usr rootfs.tmp/ ${CODENAME} ${DATE}
	debuerreotype-init --no-merged-usr rootfs-slim.tmp/ ${CODENAME} ${DATEF}
		# debuerreotype acepta parámetrtos de debootstrap --no-merged-usr
	echo ErrorCode:$?
	# http://snapshot.debian.org/
	debuerreotype-minimizing-config rootfs-slim.tmp/

	# TODO: Borra /usr/share/man/man* esto rompe man si se quiere usar en el futuro, también rompe jre java.
	# revisar rootfs/etc/dpkg/dpkg.cfg.d/docker
	# path-exclude /usr/share/man/*
	debuerreotype-slimify rootfs-slim.tmp/
	find rootfs-slim.tmp/var/cache/apt/ -type f -exec rm {} \;
	find rootfs-slim.tmp/var/lib/apt/lists/ -type f -exec rm {} \;

#	mv rootfs.tmp rootfs


	# tar -cC rootfs/ . | tar -xC rootfs-slim/

push: build

	podman push registry.conexo.mx/kaxtli/debian:${CODENAME}
	podman push registry.conexo.mx/kaxtli/debian:${CODENAME}-${DATE}
	podman push registry.conexo.mx/kaxtli/debian:${CODENAME}-slim
        podman push registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-${ARCH}
        podman push registry.conexo.mx/kaxtli/debian:${CODENAME}-slim-$(shell date --date @$(shell cat rootfs-slim.tmp/debuerreotype-epoch ) +%Y%m%d )


#debootstrap:
rootfs-full.tmp:
	# bookworm usará oficialmente merged-usr
	debootstrap --no-merged-usr --variant=minbase ${CODENAME} rootfs-full.tmp/

clean:
	rm -fr cache/ rootfs-slim.tmp/ rootfs-full.tmp/ rootfs.tmp

